#!/usr/bin/env iocsh.bash

# All require need to have version number 
require(julabof25hl)

# Set parameters when not using auto deployment
epicsEnvSet(IPADDR,     "192.168.2.45:COM1")
epicsEnvSet(IPPORT,     "4003")
epicsEnvSet(PREFIX,     "SES-HGNSANS-01:WTctrl-JUL25HL-001")
epicsEnvSet(PORTNAME,   "$(PREFIX)")
epicsEnvSet(TEMPSCAN,   "10")
epicsEnvSet(SETPNAME,   "SP")
epicsEnvSet(SETPNR,     "11")
epicsEnvSet(LOCATION,   "V20; $(IPADDR)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(julabof25hl_DIR)db")

#Use for Kameleon sim
#epicsEnvSet(IPADDR, "127.0.0.1") #For use with Chameleon simulator
#epicsEnvSet(IPPORT, "9999")

#Specifying the TCP endpoint and port name
#drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

#Specifying the Serial endpoint
drvAsynSerialPortConfigure ("$(PORTNAME)", "/dev/ttyS0")
asynSetOption ("$(PORTNAME)", 0, "baud", "9600")
asynSetOption ("$(PORTNAME)", 0, "bits", "7")
asynSetOption ("$(PORTNAME)", 0, "parity", "even")
asynSetOption ("$(PORTNAME)", 0, "stop", "1")
asynSetOption ("$(PORTNAME)", 0, "clocal", "N")
asynSetOption ("$(PORTNAME)", 0, "crtscts", "Y")

# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

#Load your database defining the EPICS records
iocshLoad("$(julabof25hl_DIR)/julabof25hl.iocsh", "P=$(PREFIX), PORT=$(PORTNAME), ADDR=$(IPPORT)")
